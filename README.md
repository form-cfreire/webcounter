# Webcounter
Simple Python Webcounter with redis server

> By 2024-07-22 | César Freire

## Manual operations

### Build
    docker build -t webcounter .

### Dependencies
    docker run -d --name redis -p 6379:6379 -v redis-cache:/data redis:alpine redis-server --save 10 1

### Run
    docker run -d --name webcounter -p 8000:5000 --link redis -e REDIS_URL=redis webcounter


----------------


## Docker Playground example

### Instal gitlab-runner

    sudo apk add gitlab-runner

### Gitlab register

        GitLab | CI/CD Settings | Runners | Project runners

__example__

```sh
gitlab-runner register  --url https://gitlab.com  --token glrt-iwX38TBMqj-8Fg9yyBE4
```

### Run runner

    gitlab-runner run


--------------------


## CI/CD Operations

### Install gitlab runner
https://www.fosstechnix.com/install-gitlab-runner-on-ubuntu-22-04-lts/

    curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash

    sudo apt-get install gitlab-runner

    sudo usermod -a -G docker gitlab-runner

    sudo systemctl restart gitlab-runner

### Register gitlab-runner

__example__

```sh
gitlab-runner register  --url https://gitlab.com  --token glrt-iwX38TBMqj-8Fg9yyBE4
```

### Create a docker swarm cluster

    docker swarm init
    
### Monitor progress

    journalctl -f -u gitlab-runner

## Deploy stack

    docker stack deploy -c docker-compose.yml app


### Verify stack

    docker stack ps app

### Stop stack

    docker stack rm app