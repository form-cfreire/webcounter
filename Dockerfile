FROM python:3.11-alpine

# Set environment variables
ENV PIP_DISABLE_PIP_VERSION_CHECK 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

ARG REDIS_URL='localhost'
ENV REDIS_URL=${REDIS_URL}

ARG CI_COMMIT_SHORT_SHA='dev'
ENV CI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA}

ARG WEBCOUNTER_STRESS=1000
ENV WEBCOUNTER_STRESS=${WEBCOUNTER_STRESS}

EXPOSE 5000

RUN adduser -D user
USER user
WORKDIR /home/user
ENV PATH="/home/user/.local/bin:${PATH}"

COPY --chown=user:user requirements.txt .
RUN pip install --no-cache-dir --user -r requirements.txt
COPY --chown=user:user . .

CMD ["python", "app.py"]