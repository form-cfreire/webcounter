import time
from math import sqrt
from os import environ
import redis
from flask import Flask, render_template, jsonify, request
from waitress import serve


# Ambient variables

# URL for redis service or container
redis_url = environ["REDIS_URL"] if "REDIS_URL" in environ else 'localhost'
# HASH of git version from GitLab
git_version = environ["CI_COMMIT_SHORT_SHA"] if "CI_COMMIT_SHORT_SHA" in environ else "dev"

# Loops to force high cpu load
stress_interval = environ['WEBCOUNTER_STRESS'] if "WEBCOUNTER_STRESS" in environ else 1000

# Get HOSTNAME
hostname = environ['HOSTNAME'] if "HOSTNAME" in environ else 'localhost'


# Global variables
app = Flask(__name__)
cache = redis.Redis(host=redis_url, port=6379)
local_hits = 0


@app.route('/')
def index():
    ''' Render welcome entry page '''
    return render_template('index.html', version=git_version, hostname=hostname, ip=request.remote_addr)

# 
@app.route('/local')
def get_local_hits():
    ''' JSON with 'hits' local variable '''
    global local_hits
    local_hits += 1
    return jsonify({'hits:': local_hits, 'type': 'local'})

def redis_get_count():
    ''' Get and increment hits counter'''
    retries = 3
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.3)
            

@app.route('/hits')
def redis_hits():
    ''' JSON with remote redis 'hits' '''
    try:
        hits = redis_get_count()
    except:
        return jsonify({'error': f'Redis server not found at url: {redis_url}'})
    return jsonify({'hits:': hits, 'type': 'redis'})


@app.route('/load')
def cpu_load():
    ''' Create cpu load '''
    start_time = time.time()
    for _ in range(int(stress_interval) * 100_000):
         sqrt(64*64*64*64*64*64*64)
    return jsonify({'time:':round((time.time() - start_time) ,3), 'unit':'sec'})



if __name__ == '__main__':
    print(f'Starting webcounter | version:{git_version}')
    serve(app, listen='*:5000', threads=4)